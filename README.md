# webpack dev react
Simple react hot reloading via webpack dev

## running
Run webpack dev server, serve out of `./build`
```sh
npm run start
```

## notes
Webpack setup taken in large part from [here](http://survivejs.com/webpack/developing-with-webpack/getting-started/)
