import React from 'react';
import ReactDOM from 'react-dom'
import Hello from './component.jsx';

main();

function main() {
  console.log('main called')
  ReactDOM.render(<Hello />, document.getElementById('app'));
}
